use crate::commands::command_dispatch;
use matrix_sdk::{
    Client,
    EventHandler,
    api::r0::message::send_message_event::Response,
    async_trait,
    events::{
        AnyMessageEventContent::RoomMessage,
        StrippedStateEvent,
        SyncMessageEvent,
        room::{
            member::MemberEventContent,
            message::{
                EmoteMessageEventContent,
                MessageEventContent,
                MessageType,
                TextMessageEventContent,
            },
        },
    },
    identifiers::{DeviceId, UserId},
    room::{Joined, Room},
};
use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;
use std::convert::TryFrom;
use std::error::Error as StdError;
use tokio::time::{sleep, Duration};
use url::Url;

/// Hold and instance of the bot
#[derive(Debug,Clone)]
pub struct PurpleBot {
    pub client: Client,
    pub homeserver: Url,
    pub access_token: String,
    pub user_id: UserId,
    pub device_id: Box<DeviceId>,
    pub database: Pool<SqliteConnectionManager>,    
}

impl PurpleBot {

    /// Return bot instances for each configuration row in the database.
    ///
    /// # Arguments
    ///
    /// * `database` - A r2d2 sqlite connection manager pool
    pub fn from_db(database: Pool<SqliteConnectionManager>)
                   -> Result<Vec<Self>, Box<dyn StdError>> {
        let query = "SELECT homeserver_url, access_token, user_id, device_id \
                     FROM matrix_login;";

        let conn = database.get().unwrap();
        let mut stmt = conn.prepare(&query)?;
        let mut configs: Vec<PurpleBot> = Vec::new();

        let rows = stmt.query_map([],|row| {
            Ok(Self::new(&row.get::<_,String>(0)?,
                         &row.get::<_,String>(1)?,
                         &row.get::<_,String>(2)?,
                         &row.get::<_,String>(3)?,
                         database.clone()))
        })?;
        
        for config in rows {
            configs.push(config??)
        }
        
        Ok(configs)
    }

    /// Return an instance of the bot
    ///
    /// # Arguments
    ///
    /// * `homeserver` - The url of the homeserver
    /// * `access_token` - The matrix access token
    /// * `user_id` - The user id for the bot
    /// * `device_id` - The device id of the bot
    /// * `database` - A r2d2 sqlite connection manager pool
    pub fn new(homeserver: &str, access_token: &str, user_id: &str,
               device_id: &str, database: Pool<SqliteConnectionManager>)
               -> Result<Self, Box<dyn StdError>> {
        let homeserver = Url::parse(&homeserver)?;
        Ok(Self {
            client: Client::new(homeserver.clone())?,
            homeserver,
            access_token: access_token.to_owned(),
            user_id: UserId::try_from(user_id)?,
            device_id: Box::<DeviceId>::from(device_id),
            database
        })
    }
}

#[async_trait]
impl EventHandler for PurpleBot {
    async fn on_room_message(&self, room: Room,
                             event: &SyncMessageEvent<MessageEventContent>) {
        if let Room::Joined(room) = room {
            let SyncMessageEvent {
                content: MessageEventContent {
                    msgtype,
                    //relates_to,
                    //new_content,
                    ..
                },
                event_id,
                sender,
                //origin_server_ts: server_ts,
                //unsigned,
                ..
            } = event;
            match msgtype {
                // Process text messages to search for commands
                MessageType::Text(TextMessageEventContent{body,..}) => {
                    command_dispatch(self,event_id,&room,sender,body).await;
                },
                MessageType::Audio(_) => {},
                MessageType::Emote(_) => {},
                MessageType::File(_) => {},
                MessageType::Image(_) => {},
                MessageType::Location(_) => {},
                MessageType::Notice(_) => {},
                MessageType::ServerNotice(_) => {},
                MessageType::Video(_) => {},
                MessageType::VerificationRequest(_) => {},
                MessageType::_Custom(_) => {}
            }
        }
    }

    async fn on_stripped_state_member(&self, room:Room,
                                      room_member: &StrippedStateEvent<MemberEventContent>,
                                      _: Option<MemberEventContent>) {
        if room_member.state_key != self.client.user_id().await.unwrap() {
            return;
        }
        
        if let Room::Invited(room) = room {
            let mut delay = 2;

            while let Err(err) = room.accept_invitation().await {
                // retry autojoin due to synapse sending invites, before the
                // invited user can join for more information see
                // https://github.com/matrix-org/synapse/issues/4345
                
                sleep(Duration::from_secs(delay)).await;
                delay *= 2;

                if delay > 3600 {
                    eprintln!("Can't join room {} ({:?})", room.room_id(), err);
                    break;
                }
            }
        }
    }
}

/// A function to simplify sending html messages to a room
///
/// # Arguments
///
/// * `r` - The room to send the message to
/// * `html` - The html encoded message
/// * `plain` - The plain text message
pub async fn send_html (r: &Joined, html: &str, plain: &str)
                        -> Result<Response, Box<dyn StdError + Send + Sync>> {
    match r.send(RoomMessage(MessageEventContent::text_html(html,plain)), None).await {
        Ok(r) => Ok(r),
        Err(e) => Err(Box::new(e)),
    }
}

/// A function to simplify sending text messages to a room
///
/// # Arguments
///
/// * `r` - The room to send the message to
/// * `msg` - The plain text message
pub async fn send_text (r: &Joined, msg: &str)
                        -> Result<Response, Box<dyn StdError + Send + Sync>> {
    match r.send(RoomMessage(MessageEventContent::text_plain(msg)), None).await {
        Ok(r) => Ok(r),
        Err(e) => Err(Box::new(e)),
    }
}

/// A function to simplify sending emotes to a room
///
/// # Arguments
///
/// * `r` - The room to send the message to
/// * `msg` - The text of the emote
pub async fn send_emote (r: &Joined, msg: &str)
                         -> Result<Response, Box<dyn StdError + Send + Sync>> {
    match r.send(
        RoomMessage(
            MessageEventContent::new(
                MessageType::Emote(EmoteMessageEventContent::plain(msg)))), None).await {
        Ok(r) => Ok(r),
        Err(e) => Err(Box::new(e)),
    }
}
