mod matrix;
mod commands;

use clap::{Arg, App};
use crate::matrix::PurpleBot;
use matrix_sdk::{SyncSettings, Session};
use r2d2_sqlite::SqliteConnectionManager;
use std::error::Error as Error;

/// PurpleBot
fn main() -> Result<(),Box<dyn Error>> {
    tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .max_blocking_threads(2)
        .build()
        .unwrap()
        .block_on(async {
            let matches = App::new("PurpleBot")
                .version("0.1.0")
                .author("Jeffrey Stoffers <jstoffers@uberpurple.com>")
                .about("Purplebot is a very purple matrix chatbot")
                .arg(Arg::with_name("database")
                     .short("d")
                     .long("database")
                     .value_name("FILE")
                     .help("Set the sqlite database for purplebot to query.\n\
                            Uses value of ./database.sqlite by default.")
                     .takes_value(true))
                .get_matches();

            // Default Options for Command Line Flags
            let database = matches.value_of("database").unwrap_or("./database.sqlite");

            // Start Database Connection Pool
            let manager = SqliteConnectionManager::file(database);
            let pool = r2d2::Pool::new(manager).unwrap();
            
            // Load Matrix Configurations from the Database
            let matrix = PurpleBot::from_db(pool)?;

            // For Every Configuration Start a New Bot
            for instance in matrix {
                let client = instance.client.clone();
                
                client.restore_login(Session{
                    access_token: instance.access_token.clone(),
                    user_id: instance.user_id.clone(),
                    device_id: instance.device_id.clone(),
                }).await?;
                
                client.sync_once(SyncSettings::default()).await?;
                client.set_event_handler(Box::new(instance)).await;
                client.sync(SyncSettings::new()).await;
            }
            
            Ok(())        
        })
}
