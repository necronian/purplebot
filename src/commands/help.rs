use crate::commands::{
    Command,
    Stock,
    Do,
    IgnoreMe,
};
use crate::matrix::send_text;
use matrix_sdk::async_trait;
use std::error::Error;

#[async_trait]
pub trait Help<'a,'b,'c,'d> {
    async fn help(&self) -> Result<(), Box<dyn Error + Send + Sync>>;
    fn help_help(&self) -> String;
}

#[async_trait]
impl<'a,'b,'c,'d> Help<'a,'b,'c,'d> for Command<'a,'b,'c,'d> {
    async fn help(&self) -> Result<(), Box<dyn Error + Send + Sync>> {
                if !self.options.is_empty() {
                    match self.options[0].as_str() {
                        "stock" => send_text(self.room,&self.stock_help()).await?,
                        "do" =>send_text(self.room,&self.do_help()).await?,
                        "ignoreme" => send_text(self.room,&self.ignoreme_help()).await?,
                        "help" => send_text(self.room,&self.help_help()).await?,
                        _ => send_text(self.room,"Invalid command specified").await?,
                    };
                } else {
                    send_text(self.room,&self.help_help()).await?;
                }
        Ok(())
    }
    
    fn help_help(&self) -> String {
        if self.options.len() <= 1 {
            "!help - Get help\
             \n  Commands\
             \n  stock, do, ignoreme, help".to_string()
        } else {
            "!help(cmd [subcommand])\
             \n  Get help\
             \n  cmd - The command to get the help of\
             \n  subcommand - (Optional) get the help for a subcommand".to_string()
        }
    }
}
