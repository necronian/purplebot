use chrono::{Utc, DateTime};
use crate::commands::Command;
use crate::matrix::{send_text,send_html};
use regex::Regex;
use rusqlite::params;
use scraper::{Html, Selector};
use std::cmp::Ordering;
use std::{error::Error, fmt::{Display, Formatter, Result as fmtResult}};
use matrix_sdk::async_trait;

#[async_trait]
pub trait Stock<'a,'b,'c,'d> {
    async fn stock(&self) -> Result<(), Box<dyn Error + Send + Sync>>;
    fn stock_help(&self) -> String;
}

#[async_trait]
impl<'a,'b,'c,'d> Stock<'a,'b,'c,'d> for Command<'a,'b,'c,'d> {
    /// Checks for stock
    async fn stock(&self) -> Result<(), Box<dyn Error + Send + Sync>> {
        if !self.options.is_empty() {
            match self.options[0].as_str() {
                "add" => match add(self) {
                    Ok(_) => send_text(self.room,"New stock check item added sucsessfully.").await?,
                    Err(e) => send_text(self.room,&format!("There was a problem. {:?}",e)).await?,
                },
                "check" => match check(self).await {
                    Ok(in_stock) => if !in_stock.is_empty() {
                        send_html(self.room, &stock_to_html_table(&in_stock), &stock_to_plain_table(&in_stock)).await?
                    } else {
                        send_text(self.room,"Nothing in stock").await?
                    },
                    Err(e) => send_text(self.room,&format!("There was a problem. {:?}",e)).await?,
                },
                "clone" => match clone(self) {
                    Ok(_) => send_text(self.room,"Successfully cloned entry.").await?,
                    Err(e) => send_text(self.room,&format!("There was a problem. {:?}",e)).await?,
                },
                "delete" => match delete(self) {
                    Ok(_) => send_text(self.room,"Deleted stock source successfully.").await?,
                    Err(e) => send_text(self.room,&format!("There was a problem. {:?}",e)).await?,
                },
                "edit" => match edit(self) {
                    Ok(_) => send_text(self.room,"Successfully edited entry.").await?,
                    Err(e) => send_text(self.room,&format!("There was a problem. {:?}",e)).await?,
                },
                "fetch" => match fetch(self).await {
                    Ok(stock) => send_html(self.room, &stock_to_html_table(&stock), &stock_to_plain_table(&stock)).await?,
                    Err(e) => send_text(self.room,&format!("There was a problem. {:?}",e)).await?,
                },
                "help" => match fetch(self).await {
                    Ok(_) => send_text(self.room,&self.stock_help()).await?,
                    Err(e) => send_text(self.room,&format!("There was a problem. {:?}",e)).await?,
                },
                "sources" => match StockFetch::lookup_distinct(self) {
                    Ok(sources) => send_html(self.room, &sources_to_html_table(&sources), &sources_to_plain_table(&sources)).await?,
                    Err(e) => send_text(self.room,&format!("There was a problem. {:?}",e)).await?,
                },
                "start" => match start(self).await {
                    Ok(_) => send_text(self.room,"Successfully Started").await?,
                    Err(e) => send_text(self.room,&format!("There was a problem. {:?}",e)).await?,
                },
                "status" => match status(self) {
                    Ok(_) => send_text(self.room,"Successfully added status").await?,
                    Err(e) => send_text(self.room,&format!("There was a problem. {:?}",e)).await?,
                },
                _ => send_text(self.room,"Unknown Sub-Command").await?,
            };
        } else {
            send_text(self.room,&self.stock_help()).await?;
        };
        Ok(())
    }

    /// Get help string
    fn stock_help(&self)  -> String {
        if self.options.len() <= 1 {
            "!stock - Commands for checking if web stores have stock.\
             \n  SubCommands\
             \n  add - Add a new stock item to check\
             \n  check - Check stock status of tracked items\
             \n  clone - Create a new stock check item from an existing one\
             \n  delete - Delete a stock checking item\
             \n  edit - Modify a stock checking item\
             \n  fetch - Fetch all stock statuses and display them\
             \n  sources - Show all stock items currently being checked\
             \n  start - Start the periodic stock checker\
             \n  status - Map an in stock string to a in stock true/false value".to_string()
        } else {
            match self.options[1].as_str() {
                "add" => "!stock(add) \"source\" \"name\" \"price\" \"status\" \"item\"\
                          \n  Add a new stock item to check, each section must be contained within\
                          \n    double quotes \"\"\
                          \n  \"source\" - A url pointing to the stack page for the item\
                          \n  \"name\" - A css selector to extract the name of the item\
                          \n  \"price\" - A css selector to extract the price of the item\
                          \n  \"status\" - A css selector to extract the status of the item".to_string(),
                "check" => "!stock(check)\
                            \n  Check stock status of all tracked items. Returns a list of\
                            \n    in stock items. If the meaning of any returned stock strings\
                            \n    is unknown it will prompt for adding that status type to the\
                            \n    database.".to_string(),
                "clone" => "!stock(clone url item) newurl\
                            \n  Create a new stock check item from an existing one. Given an existing url\
                            \n    and an item number. A new stock check using the new url and copy\
                            \n    the existing selectors.\
                            \n  url - the url of an existing check item\
                            \n  item - the number of an existing check item (Used if checking multiple\
                            \n         statuses on a single page. Otherwise 0)\
                            \n  newurl - the url of the new stock page to check".to_string(),
                "delete" => "!stock(delete url item)\
                             \n  Delete a stock checking item\
                             \n  url - the url of an existing check item\
                             \n  item - the number of an existing check item (Used if checking multiple\
                             \n         statuses on a single page. Otherwise 0)".to_string(),
                "edit" => "!stock(edit url item selector) newselector\
                           \n  Edit the css selector of a stock item\
                           \n  url - the url of an existing check item\
                           \n  item - the number of an existing check item (Used if checking multiple\
                           \n  selector - Which css selector to edit must be one of name, price, status\
                           \n  newselector - The new value of the css selector".to_string(),
                "fetch" => "!stock(fetch)\
                            \n  Fetch all stock statuses and display them".to_string(),
                "sources" => "!stock(sources)\
                              \n  Show all stock items currently being checked".to_string(),
                "start" => "!stock(start time)\
                            \n Start the periodic stock checker\
                            \n  time - Number of seconds to wait between checking stock".to_string(),
                "status" => "!stock(status) \"url\" \"name\" \"status\" \"boolean\"\
                             \n  Map an in stock string to a in stock true/false value. The stock checker\
                             \n    does not understand the result of scraping a website so we must manually\
                             \n    tell it if the returned result means an item is in or out of stock. The\
                             \n    quotes around the items are required.\
                             \n  url - The url of the item we are setting a status for\
                             \n  name - The name of the item we are checking the status of\
                             \n  status - The status string returned by the scraper\
                             \n  boolean - A true, false response telling the bot if that status\
                             \n            response string is a true or false value".to_string(),
                _ => "Unrecognized subcommand".to_string(),
            }
        }
    }
}

/// Holds information related to the status of a stock item
#[derive(Debug)]
struct StockItem {
    source: String,
    name: String,
    price: String,
    status: String,
    date: DateTime<Utc>,
}

/// Holds information required for fetching a stock item
#[derive(Debug)]
struct StockFetch {
    source: String,
    item: String,
    name_selector: String,
    price_selector: String,
    status_selector: String
}

/// Holds information required for determining if a stock stats means it is in
/// stock
#[derive(Debug)]
struct StockStatus {
    source: String,
    name: String,
    status: String,
    r#bool: String,
}

impl StockStatus {
    /// Insert a new stock status row into the stock_status table
    ///
    /// #Arguments
    ///
    /// * `cmd` - A Command object containing the information
    fn insert(&self, cmd: &Command<'_,'_,'_,'_>)
              -> Result<usize, Box<dyn Error + Send + Sync>> {
        let conn = cmd.bot.database.get()?;
        let query = "INSERT INTO stock_status (source, name, status, bool) \
                     VALUES (?1,?2,?3,?4)";
        match conn.execute(query, params![self.source, self.name, self.status,
                                          self.r#bool]) {
            Ok(r) => Ok(r),
            Err(e) => Err(Box::new(e))
        }
    }
}

// Format a StockItem for displaying in send_text
impl Display for StockItem {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmtResult {
        write!(f,"{}\n{}\n{} - {} - {}\n",
               self.source, self.name, self.status, self.price,
               self.date.format("%m-%d %H:%M"))
    }
}

impl StockItem {
    /// Insert a new stock item row into the stock_item table
    ///
    /// #Arguments
    ///
    /// * `cmd` - A Command object containing the information
    fn insert(&self, cmd: &Command<'_,'_,'_,'_>)
              -> Result<usize, Box<dyn Error + Send + Sync>> {
        let conn = cmd.bot.database.get()?;
        let query = "INSERT INTO stock_item (source, name, price, status, date) \
                     VALUES (?1,?2,?3,?4,?5)";
        match conn.execute(query, params![self.source,self.name,self.price,
                                          self.status,self.date]) {
            Ok(r) => Ok(r),
            Err(e) => Err(Box::new(e))
        }
    }

    /// Check the status of a stock item returning a bool representing if it
    /// is in stock. Queries the stock_status table to check if the source,
    /// name and status of the StockItem has a relevant stock status bool
    ///
    /// #Arguments
    ///
    /// * `cmd` - A Command object containing the information
    fn status(&self, cmd: &Command<'_,'_,'_,'_>)
              -> Result<Option<bool>, Box<dyn Error + Send + Sync>> {
        let conn = cmd.bot.database.get()?;
        let query = "SELECT bool FROM stock_status \
                     WHERE source = ?1 AND name = ?2 AND status = ?3";
        let row = conn.query_row(query, [&self.source,&self.name,&self.status],
                                 |row| { Ok(row.get::<_,String>(0).unwrap())});

        match row {
            Ok(b) => match b.as_str() {
                "false" => Ok(Some(false)),
                "true" => Ok(Some(true)),
                _ => Ok(None)
            }
            Err(_) => Ok(None)
        }
    }
}

impl StockFetch {
    /// Create a new StockFetch instance without selector data the lookup
    /// function can then be used to populate the selectors from the database
    ///
    /// # Arguments
    ///
    /// * `source` - The url of the stock item source page
    /// * `item` - The item number of the stock item on the page
    fn lookup_object(source: &str, item: &str) -> Self{
        Self {
            source: source.to_string(),
            item: item.to_string(),
            name_selector: "".to_string(),
            price_selector: "".to_string(),
            status_selector: "".to_string(),
        }
    }

    /// Populate the StockFetch instance with selector data from the
    /// stock_fetch table in the database
    ///
    /// #Arguments
    ///
    /// * `cmd` - A Command object containing the information
    fn lookup(mut self, cmd: &Command<'_,'_,'_,'_>)
              -> Result<Self, Box<dyn Error + Send + Sync>> {
        let conn = cmd.bot.database.get()?;
        let query = "SELECT name, price, status FROM stock_fetch \
                     WHERE source = ?1 AND item = ?2";
        let row = conn.query_row(query,[cmd.options[1].clone(),
                                        cmd.options[2].clone()],
                                 |row| { Ok((row.get(0)?,
                                             row.get(1)?,
                                             row.get(2)?))});
        if let Ok(row) = row {
            self.name_selector = row.0;
            self.price_selector = row.1;
            self.status_selector = row.2;
        };

        Ok(self)
    }

    /// Returns a Vec of StockFetch items for each row in the stock_fetch table
    ///
    /// #Arguments
    ///
    /// * `cmd` - A Command object containing the information
    fn lookup_all(cmd: &Command<'_,'_,'_,'_>)
                  -> Result<Vec<Self>, Box<dyn Error + Send + Sync>> {
        let conn = cmd.bot.database.get()?;
        let query = "SELECT source, name, price, status, item FROM stock_fetch;";

        let mut stmt = conn.prepare(&query)?;
        let rows = stmt.query_map([],|row| {
            Ok(StockFetch {source: row.get(0)?,
                           item: row.get(4)?,
                           name_selector: row.get(1)?,
                           price_selector: row.get(2)?,
                           status_selector: row.get(3)?})})?;

        let mut sources: Vec<StockFetch> = Vec::new();
        for row in rows { sources.push(row?); }

        Ok(sources)
    }

    /// Returns a Vec of StockFetch objects with only the source and item
    /// fields pouplated for every uniqe item in the stock_fetch table
    ///
    /// #Arguments
    ///
    /// * `cmd` - A Command object containing the information
    fn lookup_distinct(cmd: &Command<'_,'_,'_,'_>)
                       -> Result<Vec<Self>, Box<dyn Error + Send + Sync>> {
        let conn = cmd.bot.database.get()?;
        let query = "SELECT DISTINCT source, item FROM stock_fetch;";

        let mut stmt = conn.prepare(&query)?;
        let rows = stmt.query_map([],|row| {
            Ok(StockFetch::lookup_object(&row.get::<_,String>(0)?,
                                         &row.get::<_,String>(1)?))})?;

        let mut sources: Vec<Self> = Vec::new();
        for row in rows { sources.push(row?); }

        Ok(sources)
    }

    /// Scrape the web page using the StockFetch object data and return a
    /// StockItem with the stock status information
    async fn scrape(&self) -> Result<StockItem, Box<dyn Error + Send + Sync>> {
        let client = reqwest::Client::builder()
            .user_agent("Mozilla/5.0 (X11; Linux x86_64; rv:60.0)\
                         Gecko/20100101 Firefox/81.0")
            .timeout(std::time::Duration::from_secs(60))
            .cookie_store(true)
            .build()?;
        let resp = client.get(&self.source).send().await?;

        if !resp.status().is_success(){
            return Err(Box::from(format!("HTTP Response Error {} {}",
                                         self.source,
                                         resp.status().as_str())))
        };

        let html = Html::parse_document(&resp.text().await?);
        let name_selector = Selector::parse(&self.name_selector).unwrap();
        let price_selector = Selector::parse(&self.price_selector).unwrap();
        let status_selector = Selector::parse(&self.status_selector).unwrap();

        let name = html.select(&name_selector).next().unwrap()
            .text().collect::<Vec<_>>().join("").trim().to_string();
        let price = html.select(&price_selector).next().unwrap().text()
            .collect::<Vec<_>>().join("").trim().to_string();
        let status = html.select(&status_selector).next().unwrap().text()
            .collect::<Vec<_>>().join("").trim().to_string();

        Ok(StockItem{
            source: self.source.clone(),
            name,
            price,
            status,
            date: Utc::now(),
        })
    }

    /// Delete a stock_fetch row from the database to stock checking for it
    ///
    /// #Arguments
    ///
    /// * `cmd` - A Command object containing the information
    fn delete(&self, cmd: &Command<'_,'_,'_,'_>)
              -> Result<usize, Box<dyn Error + Send + Sync>> {

        let conn = cmd.bot.database.get()?;
        match conn.execute("DELETE FROM stock_fetch WHERE source = ?1 AND item = ?2;",
                           params![self.source,self.item]) {
            Ok(r) => Ok(r),
            Err(e) => Err(Box::new(e))
        }
    }

    /// Insert a stock_fetch row from the database to start checking stock for it
    ///
    /// #Arguments
    ///
    /// * `cmd` - A Command object containing the information
    fn insert(&self, cmd: &Command<'_,'_,'_,'_>)
              -> Result<usize, Box<dyn Error + Send + Sync>> {
        let conn = cmd.bot.database.get()?;
        match conn.execute("INSERT INTO stock_fetch \
                            (source, name, price, status, item) \
                            VALUES (?1,?2,?3,?4,?5)",
                           params![self.source, self.name_selector,
                                   self.price_selector, self.status_selector,
                                   self.item] ) {
            Ok(r) => Ok(r),
            Err(e) => Err(Box::new(e))
        }
    }
}

/// Delete a stock item from the database to stop checking for stock
fn delete(cmd: &Command<'_,'_,'_,'_>)
          -> Result<usize, Box<dyn Error + Send + Sync>> {
    match cmd.options.len().cmp(&3) {
        Ordering::Greater => Err(Box::from("Too many options supplied.")),
        Ordering::Less => Err(Box::from("Not enough options supplied.")),
        Ordering::Equal => StockFetch::lookup_object(&cmd.options[1],
                                                     &cmd.options[2])
            .delete(&cmd),
    }
}

/// Copy an existing stock source so the same selector information can be used
/// on a new url
fn clone(cmd: &Command<'_,'_,'_,'_>)
         -> Result<usize, Box<dyn Error + Send + Sync>> {
    match cmd.options.len().cmp(&3) {
        Ordering::Greater => Err(Box::from("Too many options supplied.")),
        Ordering::Less => Err(Box::from("Not enough options supplied.")),
        Ordering::Equal => {
            let orig = StockFetch::lookup_object(&cmd.options[1],
                                                 &cmd.options[2])
                .lookup(&cmd)?;

            StockFetch{
                source: cmd.message.clone(),
                name_selector: orig.name_selector,
                price_selector: orig.price_selector,
                status_selector: orig.status_selector,
                item: cmd.options[2].clone(),
            }.insert(&cmd)
        }
    }
}

/// Update selector data for a stock source
fn edit(cmd: &Command<'_,'_,'_,'_>)
        -> Result<usize, Box<dyn Error + Send + Sync>> {

    match cmd.options.len().cmp(&4) {
        Ordering::Greater => Err(Box::from("Too many options supplied.")),
        Ordering::Less => Err(Box::from("Not enough options supplied.")),
        Ordering::Equal => {
            let conn = cmd.bot.database.get()?;
            let query = match cmd.options[3].as_str() {
                "name" => "UPDATE stock_fetch SET name = ?3 WHERE source = ?1 AND item = ?2",
                "price" => "UPDATE stock_fetch SET price = ?3 WHERE source = ?1 AND item = ?2",
                "status" => "UPDATE stock_fetch SET status = ?3 WHERE source = ?1 AND item = ?2",
                _ => return Err(Box::from("Invalid selector specified.")),
            };

            match conn.execute(query,
                               params![cmd.options[1].as_str(),
                                       cmd.options[2].as_str(),
                                       cmd.message] ) {
                Ok(r) => Ok(r),
                Err(e) => Err(Box::new(e))
            }
        }
    }
}

/// Add a new stock status to the database so the stock checker knows how to
/// interpret the status strings returned from the check source
fn status(cmd: &Command<'_,'_,'_,'_>)
       -> Result<usize, Box<dyn Error + Send + Sync>> {
    let rgx = Regex::new(
        r#"\s*"(?P<source>[^"]*)"\s*"(?P<name>[^"]*)"\s*"(?P<status>[^"]*)"\s*"(?P<bool>[^"]*)"\s*"#
    ).unwrap();
    let items = rgx.captures(&cmd.message);

    let items = match items {
        Some(m) => m,
        None => return Err(Box::from("Unable to parse data.")),
    };

    let source = match items.name("source") {
        Some(m) => m.as_str().to_string(),
        None => return Err(Box::from("Unable to parse status source.")),
    };

    let name = match items.name("name") {
        Some(m) => m.as_str().to_string(),
        None => return Err(Box::from("Unable to parse status name.")),
    };

    let status = match items.name("status") {
        Some(m) => m.as_str().to_string(),
        None => return Err(Box::from("Unable to parse status status.")),
    };

    let r#bool = match items.name("bool") {
        Some(m) => m.as_str().to_string(),
        None => return Err(Box::from("Unable to parse status bool.")),
    };

    Ok(StockStatus { source, name, status, r#bool }.insert(cmd)?)
}

/// Add a new stock source to be queried
fn add(cmd: &Command<'_,'_,'_,'_>)
       -> Result<usize, Box<dyn Error + Send + Sync>> {
    let rgx = Regex::new(
        r#"\s*"(?P<source>[^"]*)"\s*"(?P<name>[^"]*)"\s*"(?P<price>[^"]*)"\s*"(?P<status>[^"]*)"\s*(?P<item>\d*)?\s*"#
    ).unwrap();

    let items = rgx.captures(&cmd.message);

    let items = match items {
        Some(m) => m,
        None => return Err(Box::from("Unable to parse data.")),
    };

    let source = match items.name("source") {
        Some(m) => m.as_str().to_string(),
        None => return Err(Box::from("Unable to parse source.")),
    };

    let name_selector = match items.name("name") {
        Some(m) => m.as_str().to_string(),
        None => return Err(Box::from("Unable to parse name selector.")),
    };

    let price_selector = match items.name("price") {
        Some(m) => m.as_str().to_string(),
        None => return Err(Box::from("Unable to parse price selector.")),
    };

    let status_selector = match items.name("status") {
        Some(m) => m.as_str().to_string(),
        None => return Err(Box::from("Unable to parse status selector.")),
    };

    let item = match items.name("item") {
        Some(m) => match m.as_str() {
            "" => "0".to_string(),
            _=> m.as_str().to_string(),
        },
        None => "0".to_string(),
    };

    StockFetch {
        source,
        name_selector,
        price_selector,
        status_selector,
        item,
    }.insert(&cmd)
}

/// Fetch stock status of all stock items currently being tracked
async fn fetch(cmd: &Command<'_,'_,'_,'_>)
                -> Result<Vec<StockItem>, Box<dyn Error + Send + Sync>> {
    let mut items: Vec<StockItem> = Vec::new();
    for source in StockFetch::lookup_all(&cmd)? {
        let stock = source.scrape().await?;
        stock.insert(&cmd)?;
        items.push(stock);
    }
    Ok(items)
}

/// Render a Vector of (String,String) containing a stock source url and item
/// number to a nice looking String containing html table markup for matrix
/// clients that render html.
fn sources_to_html_table(sources: &[StockFetch]) -> String {
    let mut table = "<table><thead><tr><th>Url</th><th>Number</th></tr></thead><tbody>"
        .to_string();
    for i in sources {
        table.push_str(&format!("<tr><td>{}</td><td>{}</td></tr>",i.source,i.item));
    }
    table.push_str("</tbody></table>");
    table
}

/// Render a Vector of StockItems to a nice looking String containing html
/// table markup for matrix clients that render html.
fn stock_to_html_table(stock: &[StockItem]) -> String {
    let mut table = "<table><thead><tr><th>Item</th><th>Status</th><th>Price\
                     </th><th>Date</th></tr></thead><tbody>".to_string();
    for i in stock {
        table.push_str(&format!(
            "<tr><td><a href=\"{}\">{}</a></td><td>{}</td><td>{}</td><td>{}</td></tr>",
            i.source, i.name, i.status, i.price, i.date));
    }
    table.push_str("</tbody></table>");
    table
}

/// Render a Vector of (String,String) containing a stock source url and item
/// number to a nice looking String format for text only matrix clients.
fn sources_to_plain_table(sources: &[StockFetch]) -> String {
    let mut table: String = "".to_string();
    for i in sources { table.push_str(&format!("{} - {}\n",i.source,i.item)); }
    table
}

/// Render a Vector of StockItems to a nice looking String format for text
/// only matrix clients.
fn stock_to_plain_table(stock: &[StockItem]) -> String {
    let mut table = "".to_string();
    for i in stock { table.push_str(&i.to_string()); }
    table
}

/// Check to see if items are in stock.
///
/// If items are found which the returned stock status string isn't
/// understood, prompt the user for status information.
///
/// Otherwise returns a Vec of StockItem of items that are currently in stock.
async fn check(cmd: &Command<'_,'_,'_,'_>)
         -> Result<Vec<StockItem>, Box<dyn Error + Send + Sync>> {
    let mut in_stock:  Vec<StockItem> = Vec::new();
    let mut unknown_stock:  Vec<StockItem> = Vec::new();

    for i in fetch(cmd).await? {
        match i.status(cmd) {
            Ok(Some(true)) => in_stock.push(i),
            Ok(Some(false)) => {},
            Ok(None) => unknown_stock.push(i),
            Err(_) => {},
        }
    }

    for u in unknown_stock {
        send_text(cmd.room,&format!("{} - {}\n\
                                     Unknown stock status for item = {}\n\
                                     Please resond with the following if this \
                                     means the item is out of stock/or \
                                     modify to true for in stock",
                                    u.name,u.source,u.status)).await?;
        send_text(cmd.room,&format!("!stock(status) \"{}\" \"{}\" \"{}\" \"false\""
                                    ,u.source,u.name,u.status)).await?;
    }

    Ok(in_stock)
}

/// Start running the stock check command in the background to periodically
/// poll for new stock q
async fn start(cmd: &Command<'_,'_,'_,'_>)
               -> Result<(), Box<dyn Error + Send + Sync>> {

    // TODO: Doing this lets me ignore static lifetime issues with passing
    // cmd to task::spawn. Figure out the correct way to do this eventually.
    let seconds: u64 = cmd.options[1].parse()?;
    let bot = cmd.bot.clone();
    let event_id = cmd.event_id.clone();
    let room = cmd.room.clone();
    let sender = cmd.sender.clone();
    let command = cmd.command.clone();
    let options = cmd.options.clone();
    let message = cmd.message.clone();

    // Todo:: Figure out how to store a handle to this so it can be stopped later
    tokio::task::spawn(async move {
        let command = Command {
            bot: &bot,
            event_id: &event_id,
            room: &room,
            sender: &sender,
            command,
            options,
            message,
        };

        let mut counter: u32 = 0;

        // TODO: Figure out every way this can fail and how to return errors
        // so it can be delt with
        loop {
            tokio::time::sleep(std::time::Duration::from_secs(seconds)).await;
            if let Err(e) = match check(&command).await {
                Ok(in_stock) => if !in_stock.is_empty() {
                    send_html(command.room, &stock_to_html_table(&in_stock),
                              &stock_to_plain_table(&in_stock)).await
                } else {
                    if counter % 480 == 0 {
                        let _e = send_text(command.room,
                                           &format!("I have checked for stock {} times.",
                                                    counter)).await;
                    }
                    Err(Box::from("No Stock"))
                },
                Err(e) => send_text(command.room,
                                    &format!("There was a problem checking stock!. \n{:?}",
                                             e)).await,
            } {
                println!("Error in spawn: {:?}",e);
            };

            counter += 1;
        }

    });

    Ok(())
}

// Check why this breaks?
// stock(status) "https://www.newegg.com/gigabyte-geforce-rtx-3080-gv-n3080aorus-m-10gd/p/N82E16814932336" "GIGABYTE AORUS GeForce RTX 3080 MASTER 10GB Video Card, GV-N3080AORUS M-10GD (rev. 1.0)" "OUT OF STOCK." "false"
