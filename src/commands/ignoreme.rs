use crate::commands::Command;
use matrix_sdk::async_trait;
use std::{
    error::Error,
    fs::File,
    path::PathBuf,
};

#[async_trait]
pub trait IgnoreMe<'a,'b,'c,'d> {
    async fn ignoreme(&self) -> Result<(), Box<dyn Error + Send + Sync>>;
    fn ignoreme_help(&self) -> String;
}

#[async_trait]
impl<'a,'b,'c,'d> IgnoreMe<'a,'b,'c,'d> for Command<'a,'b,'c,'d> {
    async fn ignoreme(&self)
                       -> Result<(), Box<dyn Error + Send + Sync>> {
        let room = self.room;
        let path = PathBuf::from("./media/ignoreme.mp4");
        let mt = "video/mp4".parse::<mime::Mime>().unwrap();
        let mut video = File::open(path).expect("Can't open video file.");
        room.send_attachment("Ignore Me!", &mt, &mut video, None).await?;
        Ok(())
    }
    
    fn ignoreme_help(&self) -> String {
        if self.options.len() <= 1 {
            "!ignoreme - SOMEBODY LEFT A BABY".to_string()
        } else {
            "!ignoreme has no subcommands".to_string()
        }
    }
}
