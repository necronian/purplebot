pub mod do_cmd;
pub mod ignoreme;
pub mod stock;
pub mod help;

use crate::commands::{
    do_cmd::Do,
    help::Help,
    ignoreme::IgnoreMe,
    stock::Stock,
};
use crate::matrix::PurpleBot;
use matrix_sdk::{
    room::Joined,
    identifiers::{EventId, UserId},
};
use regex::Regex;
use std::error::Error;

/// Holds information related to a command sent to the bot
#[derive(Debug,Clone)]
pub struct Command<'a,'b,'c,'d> {
    pub bot: &'a PurpleBot,
    pub event_id: &'b EventId,
    pub room: &'c Joined,
    pub sender: &'d UserId,
    pub command: String,
    pub options: Vec<String>,
    pub message: String,
}

impl<'a,'b,'c,'d> Command<'a,'b,'c,'d> {
    /// Create a new command instance
    ///
    /// # Arguments
    ///
    /// * `bot` - An instance of a matrix bot
    /// * `event_id` - The EventId of the message that contained the command
    /// * `room` - the Joined room that the command message was sent to
    /// * `sender` - the UserId of the person who sent the message
    /// * `msg` - the contents of the message
    fn new(bot: &'a PurpleBot, event_id: &'b EventId, room: &'c Joined,
           sender: &'d UserId, msg: &str)
           -> Result<Self,Box<dyn Error + Send + Sync>> {
        let rgx = Regex::new(r"^!(?P<c>[^\s\(]+)(\((?P<o>.*)\))?\s*(?P<m>.*)")
            .unwrap();
        match rgx.captures(msg) {
            Some(msg) => {
                let command = msg.name("c").unwrap().as_str().to_string();
                let options = match msg.name("o") {
                    Some(m) => m.as_str().split(' ').map(|x| x.to_string()).collect(),
                    None => Vec::new(),
                };
                let message = msg.name("m").unwrap().as_str().trim().to_string();
                Ok(Self { bot, event_id, room, sender, command, options, message })
            },
            None => Err(Box::from("Unable to parse command"))
        }
    }
}

/// Takes a message, creates a new Command and dispatches it to the correct
/// command function.
///
/// # Arguments
///
/// * `bot` - An instance of a matrix bot
/// * `event_id` - The EventId of the message that contained the command
/// * `room` - the Joined room that the command message was sent to
/// * `sender` - the UserId of the person who sent the message
/// * `msg` - the contents of the message
pub async fn command_dispatch(bot: &PurpleBot, event_id: &EventId,
                              room: &Joined, sender: &UserId, message: &str) {
    if let Ok(cmd) = Command::new(bot,event_id,room,&sender,message) {
        if *sender != bot.user_id {
            if let Err(e) = match cmd.command.as_str() {
                "do" => cmd.r#do().await,
                "ignoreme" => cmd.ignoreme().await,
                "stock" => cmd.stock().await,
                "help" => cmd.help().await,
                _ => Ok(())
            } {
                println!("Error: {:?}",e);
            };
        };
    }
}
