use crate::commands::Command;
use crate::matrix::{send_emote, send_text};
use std::error::Error;
use matrix_sdk::async_trait;

#[async_trait]
pub trait Do<'a,'b,'c,'d> {
    async fn r#do(&self) -> Result<(), Box<dyn Error + Send + Sync>>;
    fn do_help(&self) -> String;
}

#[async_trait]
impl<'a,'b,'c,'d> Do<'a,'b,'c,'d> for Command<'a,'b,'c,'d> {
    async fn r#do(&self) -> Result<(), Box<dyn Error + Send + Sync>> {
        let content = self.message.to_lowercase();

        if content.contains("seppuku") {
            send_text(self.room,"散るをいとふ世にも人にもさきがけて 散るこそ花と吹く小夜嵐").await?;
            send_emote(self.room,&"commits seppuku").await?;
            self.room.leave().await?;
        } else {
            send_emote(self.room,&format!("does {}",self.message)).await?;
            send_text(self.room,"WHEEEEE!").await?;
        };

        Ok(())
    }

    fn do_help(&self) -> String {
        if self.options.len() <= 1 {
            "!do - Do something... happily?".to_string()
        } else {
            "!do has no subcommands".to_string()
        }
    }
}
